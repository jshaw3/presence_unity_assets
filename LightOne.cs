﻿using UnityEngine;
using System.Collections;
using System;
using System.Threading;

public class LightOne : MonoBehaviour {
	
	public bool boxOne = false;
	
	public Light myFirstLight;
	public Light mySecondLight;
	public Light myThirdLight;
	public Light myFourthLight;
	
	public int physical_one = 0;
	public int socket_one = 0;
	
	public int return_one = 0;
	public int return_two = 0;
	public int return_three = 0;
	public int return_four = 0;
	
	string[] leds;
	char[] delimiterChars = {','};
	
	void OnTriggerEnter ( Collider player){
		Debug.Log ("** Object Entered the trigger");
		
		if (GetComponent<Collider> ().name == "Collider1") {
			myFirstLight.enabled = true;
		}

		if (GetComponent<Collider> ().name == "Collider2") {
			mySecondLight.enabled = true;
		}

		if (GetComponent<Collider> ().name == "Collider3") {
			myThirdLight.enabled = true;
		}

		if (GetComponent<Collider> ().name == "Collider4") {
			myFourthLight.enabled = true;
		}
	}


	void OnTriggerExit (){
		Debug.Log ("** Object Exited the trigger");
		
		if (GetComponent<Collider> ().name == "Collider1") {
			myFirstLight.enabled = false;
		}

		if (GetComponent<Collider> ().name == "Collider2") {
			mySecondLight.enabled = false;
		}

		if (GetComponent<Collider> ().name == "Collider3") {
			myThirdLight.enabled = false;
		}

		if (GetComponent<Collider> ().name == "Collider4") {
			myFourthLight.enabled = false;
		}
		
	}
	
		// Use this for initialization
		IEnumerator Start () {
			WebSocket w1 = new WebSocket(new Uri("ws://localhost:8000/"));
			yield return StartCoroutine(w1.Connect());
	
			while (true)
			{

				string reply = w1.RecvString();
//				w1.SendString(return_one + "," + return_two + "," + return_three + "," + return_four);

				if (reply != null)
				{
	
					Debug.Log ("Ever Met?" + reply);
	
					leds = reply.Split(delimiterChars);
	
					Debug.Log("leds" +  leds);
					Debug.Log("leds length" +  leds.Length);
	
	
					if(leds.Length == 4){
	
						Debug.Log ("led 0: " + leds[0]);
	
						if(leds[0] == "1"){
							myFirstLight.enabled = true;
							socket_one = 1;
							return_one = 1;
						} else if (physical_one == 1){
							myFirstLight.enabled = true;
							return_one = 1;
						} else {
							myFirstLight.enabled = false;
							socket_one = 0;
							return_one = 0;
						}

//						if(leds[1] == "1"){
//							return_two = 1;
//							mySecondLight.enabled = true;
//						} else {
//							return_two = 0;
//							mySecondLight.enabled = false;
//						}
//
//						if(leds[2] == "1"){
//							return_three = 1;
//							myThirdLight.enabled = true;
//						} else {
//							return_three = 0;
//							myThirdLight.enabled = false;
//						}
//						
//						if(leds[3] == "1"){
//							return_four = 1;
//							myFourthLight.enabled = true;
//						} else {
//							return_four = 0;
//							myFourthLight.enabled = false;
//						}
	
					}
	
				}
				
				// w1.SendString(return_one + "," + return_two + "," + return_three + "," + return_four);

				// return_one = 0;
				// leds = "0,0,0,0".Split(delimiterChars);
			
				if (w1.error != null)
				{
					Debug.LogError ("Error: "+w1.error);
					break;
				}
				yield return 0;
			}
			w1.Close();
		}
}

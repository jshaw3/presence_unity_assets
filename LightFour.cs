﻿using UnityEngine;
using System.Collections;
using System;
using System.Threading;

public class LightFour : MonoBehaviour {
	
	public bool boxFour = false;
	
	public Light myFirstLight;
	public Light mySecondLight;
	public Light myThirdLight;
	public Light myFourthLight;
	
	public int physical_four = 0;
	public int socket_four = 0;
	
	public int return_one = 0;
	public int return_two = 0;
	public int return_three = 0;
	public int return_four = 0;
	
	string[] leds;
	char[] delimiterChars = {','};
	
	void OnTriggerEnter ( Collider player){
		
		Debug.Log ("** Object Entered the trigger");
		
		if (GetComponent<Collider> ().name == "Collider1") {
			myFirstLight.enabled = true;
		}
		
		if (GetComponent<Collider> ().name == "Collider2") {
			mySecondLight.enabled = true;
		}
		
		if (GetComponent<Collider> ().name == "Collider3") {
			myThirdLight.enabled = true;
		}
		
		if (GetComponent<Collider> ().name == "Collider4") {
			myFourthLight.enabled = true;
		}
	}
	
	
	void OnTriggerExit (){
		Debug.Log ("** Object Exited the trigger");
		
		if (GetComponent<Collider> ().name == "Collider1") {
			myFirstLight.enabled = false;
		}
		
		if (GetComponent<Collider> ().name == "Collider2") {
			mySecondLight.enabled = false;
		}
		
		if (GetComponent<Collider> ().name == "Collider3") {
			myThirdLight.enabled = false;
		}
		
		if (GetComponent<Collider> ().name == "Collider4") {
			myFourthLight.enabled = false;
		}
		
	}
	
	// Use this for initialization
	IEnumerator Start () {
		WebSocket w4 = new WebSocket(new Uri("ws://localhost:8000/"));
		yield return StartCoroutine(w4.Connect());
		
		while (true)
		{
			string reply = w4.RecvString();
			
			// w4.SendString(return_one + "," + return_two + "," + return_three + "," + return_four);
			
			if (reply != null)
			{
				
				leds = reply.Split(delimiterChars);
				
				if(leds.Length == 4){

					if(leds[3] == "1"){
						myFourthLight.enabled = true;
						socket_four = 1;
						return_four = 1;
					} else if (physical_four == 1){
						myFourthLight.enabled = true;
						return_four = 1;
					} else {
						myFourthLight.enabled = false;
						socket_four = 0;
						return_four = 0;
					}
					
					
//					if(leds[0] == "1"){
//						return_one = 1;
//					} else {
//						return_one = 0;
//					}
//					
//					if(leds[1] == "1"){
//						return_two = 1;
//						mySecondLight.enabled = true;
//					} else {
//						return_two = 0;
//						mySecondLight.enabled = false;
//					}
//					
//					if(leds[2] == "1"){
//						return_three = 1;
//						myThirdLight.enabled = true;
//					} else {
//						return_three = 0;
//						myThirdLight.enabled = false;
//					}
					
				}
				
			}
			
			// w4.SendString(return_one + "," + return_two + "," + return_three + "," + return_four);
			
			// return_one = 0;
			// leds = "0,0,0,0".Split(delimiterChars);
			
			if (w4.error != null)
			{
				Debug.LogError ("Error: "+w4.error);
				break;
			}
			yield return 0;
		}
		w4.Close();
	}
}

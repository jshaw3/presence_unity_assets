﻿using UnityEngine;
using System.Collections;
using System;
using System.Threading;

public class LightThree : MonoBehaviour {
	
	public bool boxThree = false;
	
	public Light myFirstLight;
	public Light mySecondLight;
	public Light myThirdLight;
	public Light myFourthLight;
	
	public int physical_three = 0;
	public int socket_three = 0;
	
	public int return_one = 0;
	public int return_two = 0;
	public int return_three = 0;
	public int return_four = 0;
	
	string[] leds;
	char[] delimiterChars = {','};
	
	void OnTriggerEnter ( Collider player){
		
		Debug.Log ("** Object Entered the trigger");
		
		if (GetComponent<Collider> ().name == "Collider1") {
			myFirstLight.enabled = true;
		}
		
		if (GetComponent<Collider> ().name == "Collider2") {
			mySecondLight.enabled = true;
		}
		
		if (GetComponent<Collider> ().name == "Collider3") {
			myThirdLight.enabled = true;
		}
		
		if (GetComponent<Collider> ().name == "Collider4") {
			myFourthLight.enabled = true;
		}
	}
	
	
	void OnTriggerExit (){
		Debug.Log ("** Object Exited the trigger");
		
		if (GetComponent<Collider> ().name == "Collider1") {
			myFirstLight.enabled = false;
		}
		
		if (GetComponent<Collider> ().name == "Collider2") {
			mySecondLight.enabled = false;
		}
		
		if (GetComponent<Collider> ().name == "Collider3") {
			myThirdLight.enabled = false;
		}
		
		if (GetComponent<Collider> ().name == "Collider4") {
			myFourthLight.enabled = false;
		}
		
	}
	
	// Use this for initialization
	IEnumerator Start () {
		WebSocket w3 = new WebSocket(new Uri("ws://localhost:8000/"));
		yield return StartCoroutine(w3.Connect());
		
		while (true)
		{

			// w3.SendString(0 + "," + 0 + "," + 0 + "," + physical_three);

			string reply = w3.RecvString();
			
			// w3.SendString(return_one + "," + return_two + "," + return_three + "," + return_four);
			
			if (reply != null)
			{
				
				leds = reply.Split(delimiterChars);
				
				if(leds.Length == 4){
					
					Debug.Log ("led 2: " + leds[2]);
					
					if(leds[2] == "1"){
						myThirdLight.enabled = true;
						socket_three = 1;
						return_three = 1;
					} else if (physical_three == 1){
						myThirdLight.enabled = true;
						return_three = 1;
					} else {
						myThirdLight.enabled = false;
						socket_three = 0;
						return_three = 0;
					}
					
					
//					if(leds[0] == "1"){
//						return_one = 1;
//					} else {
//						return_one = 0;
//					}
					
//					if(leds[1] == "1"){
//						return_two = 1;
//						mySecondLight.enabled = true;
//					} else {
//						return_two = 0;
//						mySecondLight.enabled = false;
//					}
//					
//					if(leds[3] == "1"){
//						return_four = 1;
//						myFourthLight.enabled = true;
//					} else {
//						return_four = 0;
//						myFourthLight.enabled = false;
//					}
					
				}
				
			}
			
			// w3.SendString(return_one + "," + return_two + "," + return_three + "," + return_four);
			
			// return_one = 0;
			// leds = "0,0,0,0".Split(delimiterChars);
			
			if (w3.error != null)
			{
				Debug.LogError ("Error: "+w3.error);
				break;
			}
			yield return 0;
		}
		w3.Close();
	}
}

﻿using UnityEngine;
using System.Collections;
using System;
using System.Threading;

public class LightTwo : MonoBehaviour {
	
	public bool boxTwo = false;
	
	public Light myFirstLight;
	public Light mySecondLight;
	public Light myThirdLight;
	public Light myFourthLight;
	
	public int physical_two = 0;
	public int socket_two = 0;
	
	public int return_one = 0;
	public int return_two = 0;
	public int return_three = 0;
	public int return_four = 0;
	
	string[] leds;
	char[] delimiterChars = {','};
	
	void OnTriggerEnter ( Collider player){
		
		Debug.Log ("** Object Entered the trigger");
		
		if (GetComponent<Collider> ().name == "Collider1") {
			myFirstLight.enabled = true;
		}
		
		if (GetComponent<Collider> ().name == "Collider2") {
			mySecondLight.enabled = true;
		}
		
		if (GetComponent<Collider> ().name == "Collider3") {
			myThirdLight.enabled = true;
		}
		
		if (GetComponent<Collider> ().name == "Collider4") {
			myFourthLight.enabled = true;
		}
	}
	
	
	void OnTriggerExit (){
		Debug.Log ("** Object Exited the trigger");
		
		if (GetComponent<Collider> ().name == "Collider1") {
			myFirstLight.enabled = false;
		}
		
		if (GetComponent<Collider> ().name == "Collider2") {
			mySecondLight.enabled = false;
		}
		
		if (GetComponent<Collider> ().name == "Collider3") {
			myThirdLight.enabled = false;
		}
		
		if (GetComponent<Collider> ().name == "Collider4") {
			myFourthLight.enabled = false;
		}
		
	}
	
	// Use this for initialization
	IEnumerator Start () {
		WebSocket w2 = new WebSocket(new Uri("ws://localhost:8000/"));
		yield return StartCoroutine(w2.Connect());
		
		while (true)
		{

			string reply = w2.RecvString();
			
			// w2.SendString(return_one + "," + return_two + "," + return_three + "," + return_four);
			
			if (reply != null)
			{
				
				leds = reply.Split(delimiterChars);
				
				if(leds.Length == 4){
					
					if(leds[1] == "1"){
						mySecondLight.enabled = true;
						socket_two = 1;
						return_two = 1;
					} else if (physical_two == 1){
						mySecondLight.enabled = true;
						return_two = 1;
					} else {
						mySecondLight.enabled = false;
						socket_two = 0;
						return_two = 0;
					}
					
					
//					if(leds[0] == "1"){
//						return_one = 1;
//						myFirstLight.enabled = true;
//					} else {
//						return_one = 0;
//						myFirstLight.enabled = false;
//					}
//					
//					if(leds[2] == "1"){
//						return_three = 1;
//						myThirdLight.enabled = true;
//					} else {
//						return_three = 0;
//						myThirdLight.enabled = false;
//					}
//					
//					if(leds[3] == "1"){
//						return_four = 1;
//						myFourthLight.enabled = true;
//					} else {
//						return_four = 0;
//						myFourthLight.enabled = false;
//					}
					
				}
				
			}
			
			// w2.SendString(return_one + "," + return_two + "," + return_three + "," + return_four);
			
			// return_one = 0;
			// leds = "0,0,0,0".Split(delimiterChars);
			
			if (w2.error != null)
			{
				Debug.LogError ("Error: "+w2.error);
				break;
			}
			yield return 0;
		}
		w2.Close();
	}
}
